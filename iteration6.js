/*Crea una función llamada swap() que reciba un array y dos parametros que sean indices del array.
La función deberá intercambiar la posición de los valores de los indices que hayamos enviado como parametro. Retorna el array resultante.*/

const exampleArray = ['Mesirve', 'Cristiano Romualdo', 'Fernando Muralla', 'Ronalguiño'];

function swap(array, paramX, paramY){
    var primerValor = array[paramX];
    var segundoValor = array[paramY];

    array[paramX]= segundoValor;
    array[paramY]= primerValor;

    return array;

}


console.log(swap(exampleArray,0,3));

